import { ApollonClientPage } from './app.po';

describe('apollon-client App', () => {
  let page: ApollonClientPage;

  beforeEach(() => {
    page = new ApollonClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
