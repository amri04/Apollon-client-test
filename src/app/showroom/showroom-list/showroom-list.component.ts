import { Component, OnInit } from '@angular/core';
import { Showroom } from '../../shared/model/showroom.model'
import { ShowroomService } from '../shared/showroom.service'
@Component({
  selector: 'app-showroom-list',
  templateUrl: './showroom-list.component.html',
  styleUrls: ['./showroom-list.component.css'],
  providers : [ShowroomService]
})
export class ShowroomListComponent implements OnInit {

  showrooms: Showroom[];
  constructor(private showroomService: ShowroomService) { }

  ngOnInit() {
    this.showroomService.getAllShowrooms()
      .subscribe(result => this.showrooms = result);
  }
}
