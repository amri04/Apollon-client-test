import { Injectable } from '@angular/core';
import { Showroom } from '../../shared/model/showroom.model';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../appSettings';
import { Observable } from "rxjs/Observable";

@Injectable()
export class ShowroomService {
  constructor(private http: HttpClient){}

  getAllShowrooms(): Observable<Showroom[]> {
   return this.http.get(AppSettings.API_ENDPOINT+'showroom');
}
  getShowroomById(id:number): Observable<Showroom>{
    return this.http.get(AppSettings.API_ENDPOINT+'showroom/'+id);
  }
}
