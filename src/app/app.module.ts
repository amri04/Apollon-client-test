import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { ArtworkDetailComponent } from './artwork-detail/artwork-detail.component';
import { RatingComponent } from './rating/rating.component';
import { PageNotFoundComponent } from './not-found.component';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { ShowroomDetailComponent } from './showroom/showroom-detail/showroom-detail.component';
import { ShowroomListComponent } from './showroom/showroom-list/showroom-list.component';
const appRoutes: Routes = [
  { path: 'showroom', component: ShowroomListComponent },
  { path: '', component: WelcomeComponent },
  { path: 'showroom-detail/:id', component: ShowroomDetailComponent },
  { path: '**', component: PageNotFoundComponent },]
@NgModule({
  declarations: [
    AppComponent,
    ArtworkDetailComponent,
    RatingComponent,
    PageNotFoundComponent,
    WelcomeComponent,
    ShowroomDetailComponent,
    ShowroomListComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
  appRoutes,
  { enableTracing: false }
),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
