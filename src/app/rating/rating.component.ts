import { Component, OnInit,Input,AfterViewInit } from '@angular/core';
import { Rating} from '../shared/model/rating.model'
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-rating',
  template: `<div class="ui star rating star-rating" data-rating="4"></div>`,
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit,AfterViewInit {
  @Input()
  rating : Rating;
  constructor() { }

  ngAfterViewInit() {
      $('.ui.rating').rating()
  }
  ngOnInit() {
  }

}
