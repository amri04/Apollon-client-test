import { Injectable } from '@angular/core';
import { Artwork } from '../shared/model/artwork.model'
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../appSettings';
import { Observable } from "rxjs/Observable";

@Injectable()
export class ArtworkService {
  constructor(private http: HttpClient){}

  getArtworks(page:number): Observable<Artwork[]> {
   return this.http.get(AppSettings.API_ENDPOINT+'/ArtWork/findall/'+page);
}
}
