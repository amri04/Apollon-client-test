import { Component, OnInit,Input,Output,AfterViewInit,EventEmitter } from '@angular/core';
import { Artwork } from '../shared/model/artwork.model'
declare var $ :any;
@Component({
  selector: 'app-artwork-detail',
  templateUrl: './artwork-detail.component.html',
  styleUrls: ['./artwork-detail.component.css']
})
export class ArtworkDetailComponent implements OnInit,AfterViewInit {
  @Input() artwork : Artwork;
  @Output() onSelected = new EventEmitter<Artwork>();
  constructor() { }

  ngAfterViewInit() {
    $('.special.cards .image').dimmer({
  on: 'hover'
  });
  }
  ngOnInit() {

  }

}
